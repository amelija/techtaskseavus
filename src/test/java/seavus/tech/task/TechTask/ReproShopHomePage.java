package seavus.tech.task.TechTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReproShopHomePage {
	WebDriver driver;
	
	By kategorije = By.xpath("//*[@id=\"header_right\"]/div[5]/ul/li[2]/a");
	
			
	public ReproShopHomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickKategorije() {
		driver.findElement(kategorije).click();
	}
	
}