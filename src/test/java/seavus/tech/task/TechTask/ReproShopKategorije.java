package seavus.tech.task.TechTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReproShopKategorije {
	WebDriver driver;
	By magneti = By.xpath("//*[@id=\"categories_block_left\"]/div/ul/li[16]/a[contains(text(),'Magneti')]");
	
	public ReproShopKategorije(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickMagneti() {
		driver.findElement(magneti).click();
	}

}