package seavus.tech.task.TechTask;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;


public class TestReproShop {
	WebDriver driver;
	ReproShopKategorije objKat;
	ReproShopHomePage objHome;
	ReproShopMagneti objMagneti;
	ReproShopCartPage objCart;
	private static String chromeDriverPath = "C:/Users/Marija/eclipse-workspace/chromedriver.exe";
	private static String firefoxDiverPath = "C:/Users/Marija/eclipse-workspace/geckodriver.exe";
	private static String ieDriverPath = "C:/Users/Marija/eclipse-workspace/IEDriverServer.exe";
	

  @BeforeMethod
  @Parameters("browser")
  public void setBrowser(String browser) { 
	  try 
      {
          System.out.printf("Opening %s browser.\n", browser);

          switch (browser)
          {
              case "firefox":
              {
            	  FirefoxProfile profile = new FirefoxProfile();
      			  profile.setPreference("security.sandbox.content.level", 5);	
      			  FirefoxOptions options = new FirefoxOptions();
      			  options.setProfile(profile);
                  System.setProperty("webdriver.gecko.driver",firefoxDiverPath);
                  driver = new FirefoxDriver(options);
                  break;
              }
              case "chrome":
              {
                  System.setProperty("webdriver.chrome.driver",chromeDriverPath);
                  driver = new ChromeDriver();
                  break;
              }
              case "ie":
              {
            	 
                  System.setProperty("webdriver.ie.driver",ieDriverPath);
                  driver = new InternetExplorerDriver();
                  break;
              }
              default:
                  break;
          }
      } 
      catch (Exception e) 
      {
          e.printStackTrace();
      }
	  
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

      
  }
  /**
   * This test will open Home page, test the title
   * Select product and add it to the cart 
   * Compare the prices in the cart and check if those are correct 
   * And it will test each page title once its open
   **/
  
  @Test(priority = 0)
  public void testHomePageAppears() {	
	  driver.get("http://www.repro-shop.com/index.php");
	  //create Home page
	  objHome = new ReproShopHomePage(driver);
	  String titleHome = driver.getTitle();
	  System.out.println("Home: "+titleHome);
	  //test title
	  Assert.assertTrue(titleHome.startsWith("Nakit & Bižuterija | Repromaterijal za izradu nakita"), "Title didn't match");

	  objHome.clickKategorije();
	  objKat = new ReproShopKategorije(driver);
	  String titleKategorije = driver.getTitle();
	  System.out.println("Kategorije: "+titleKategorije);
	  Assert.assertTrue(titleKategorije.equals("Kategorije - TR REPROSHOP"), "Title didn't match");
	  //click on Magneti
	  objKat.clickMagneti();
	  //create Magneti page
	  objMagneti = new ReproShopMagneti(driver);
	  String titleMagneti = driver.getTitle();
	  System.out.println(titleMagneti);
	  Assert.assertTrue(titleMagneti.equals("Magneti - TR REPROSHOP"), "Title didn't match");
	  //add first item 
	  objMagneti.addToCart(objMagneti.prod1, objMagneti.prod1Price);
	  
	  this.sleep();
	  //add second item
	  objMagneti.addToCart(objMagneti.prod2, objMagneti.prod2Price);
	  this.sleep();
	  //objMagneti.addToCart(objMagneti.prod3);
	  objMagneti.addToCart(objMagneti.prod4, objMagneti.prod4Price);  
	  // open cart page
	  objMagneti.goToCart();
	  // create Cart Page
	  objCart = new ReproShopCartPage(driver);
	  // test title
	  Assert.assertTrue(objCart.getTitle().equals("Pregled korpre i potvrda porudžbine - TR REPROSHOP"), "Title didn't match");
	  // check the number of items in cart
	  int itemsInCart = objCart.getItemQuantity();
	  Assert.assertTrue(itemsInCart==3, "Number of items in cart is not correct!");
	  // check prices
	  double totalPrice = objCart.getTotalPrice();
	  Assert.assertTrue(totalPrice==objMagneti.sum, "Prices didn't match!");
	  
  }
 // @Test(priority = 1)
  public void testKategorije() {
	 driver.get("http://www.repro-shop.com/index.php?id_category=1&controller=category");
	 objKat = new ReproShopKategorije(driver);
	 String titleKategorije = driver.getTitle();
	 System.out.println("Kategorije: "+titleKategorije);
	 Assert.assertTrue(titleKategorije.equals("Kategorije - TR REPROSHOP"), "Title didn't match");
  }
  
 // @Test(priority = 2)
  public void testMagneti() {
	 driver.get("http://www.repro-shop.com/index.php?id_category=103&controller=category");
	 objMagneti = new ReproShopMagneti(driver);
	 String titleMagneti = driver.getTitle();
	 System.out.println("Magneti: "+titleMagneti);
	 Assert.assertTrue(titleMagneti.equals("Magneti - TR REPROSHOP"), "Title didn't match");
  }

  @AfterMethod
  public void afterClass() {
	  driver.quit();		 
  }
  
  public void sleep() {
	  try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
  }

}
