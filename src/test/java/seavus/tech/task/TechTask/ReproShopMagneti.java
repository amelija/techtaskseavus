package seavus.tech.task.TechTask;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver;

public class ReproShopMagneti {
	WebDriver driver;
	By prod1 = By.xpath("//*[@id=\"product_list\"]/li[7]/div/div[2]/a[contains(text(),'Ubaci u korpu')]");
	By prod1Price = By.xpath("//*[@id=\"product_list\"]/li[7]/div/div[1]/span");
	By prod2 = By.xpath("//*[@id=\"product_list\"]/li[3]/div/div[2]/a[contains(text(),'Ubaci u korpu')]");
	By prod2Price = By.xpath("//*[@id=\"product_list\"]/li[3]/div/div[1]/span");
	//By prod3 = By.xpath("//*[@id=\"product_list\"]/li[4]/div/div[2]/a[contains(text(),'Ubaci u korpu')]");
	//By prod3Price = By.xpath("//*[@id=\"product_list\"]/li[4]/div/div[1]/span");
	By prod4 = By.xpath("//*[@id=\"product_list\"]/li[5]/div/div[2]/a[contains(text(),'Ubaci u korpu')]");
	By prod4Price = By.xpath("//*[@id=\"product_list\"]/li[5]/div/div[1]/span");
	By korpa1 = By.xpath("//*[@id=\"shopping_cart\"]/span[1]");
	By korpa = By.xpath("//*[@id=\"shopping_cart\"]/a");
	By biseri = By.xpath("//*[@id=\"categories_block_left\"]/div/ul/li[1]/a");
	double sum;
	
	public ReproShopMagneti(WebDriver driver) {
		this.driver = driver;
	}
	
	public void addToCart(By prod, By prodPrice) {
		driver.findElement(prod).click();
		String[] strPrices = driver.findElement(prodPrice).getText().split(" ");
		String strPrice = strPrices[0];
		double price = Double.parseDouble(strPrice);
		sum = sum + price;
	}
	
	public void goToCart() {
		driver.findElement(korpa).click();
	}

	public String getNumberOfItemsInCart() {
		System.out.println("Broj itema u korpi: "+driver.findElement(korpa).getAttribute("value"));	
		return driver.findElement(korpa).getAttribute("value");
	}
	
	
}
