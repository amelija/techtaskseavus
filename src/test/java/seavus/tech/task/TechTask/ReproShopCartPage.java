package seavus.tech.task.TechTask;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReproShopCartPage {
	WebDriver driver;
	By quantity = By.id("summary_products_quantity");
	By totalPrice = By.id("total_price");
	String title;
	
	public ReproShopCartPage(WebDriver driver) {
		this.driver = driver;
	}

	public String getTitle() {
		this.title = driver.getTitle();
		return title;
	}
	
	public int getItemQuantity() {
		int items = 0;
		String[] strings = driver.findElement(quantity).getText().split(" ");
		System.out.println(strings[0]);
		items = Integer.parseInt(strings[0]);
		return items;
	}
	
	public double getTotalPrice() {
		String[] totals = driver.findElement(totalPrice).getText().split(" ");
		String total = totals[0];
		System.out.println("Total price from cart: "+total);
		double totalPrice = Double.parseDouble(total);
		return totalPrice;
	}
}
